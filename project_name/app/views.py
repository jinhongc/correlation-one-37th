from django.http import HttpResponse
from django.template import loader

from .models import events, cities, airport_id_routes
from .graph import Graph, dijsktra

import random


def index(request):
    template = loader.get_template('app/index.html')
    context = {
        'cities': sorted(cities, key=lambda item: item['id']),
        'events': sorted(events, key=lambda item: item['id'])
    }
    return HttpResponse(template.render(context, request))

def predict(request):
    print(request)
    print(request.method)
    if request.method == 'POST':
        print(request.POST.get('city'))
        print(request.POST.get('event'))
        # print(request.POST.get('date'))
    template = loader.get_template('app/predict.html')
    # TODO search for:
    #   1. get city of event
    #   2. search all possible route from originator city to destination city
    #   3. also get time and cost by calling the model
    #   4. output the route, time and cost

    originator_city = request.POST.get('city')
    event_id = request.POST.get('event')
    # context = _search(originator_city, event_id)
    # todo: fill in context with function _search_optimal_cost
    #
    context = {
        'airports': [
              {
                "abbrev":"ALB",
                "name": "Albany International Airport",
                "city":"Albany",
                "lat":42.74812,
                "lon":-73.80298,
                "z": 1,
                "visit_order": 0,
                  "cost": 100,
                  "delay": 1000,
                  "duration": 1000
              },
              {
                "abbrev":"ALO",
                "name": "Waterloo Regional Airport",
                "city":"Waterloo",
                "lat":42.55708,
                "lon":-92.40034,
                "z": 1,
                "visit_order": 1,
                  "cost": 100,
                  "delay": 1000,
                  "duration": 1000
              }
            ]
    }
    return HttpResponse(template.render(context, request))

def _search_optimal_cost(origin_airport, destination_airport, event_name, date='2018-01-29'):
    """
    search routes from origin_city to destination_city
    :param citorigin_city:
    :param destination_city:
    :param date:
    :return:
    """
    graph = Graph()

    for x in airport_id_routes:
        graph.add_node(x)
        for y in airport_id_routes[x]:
            graph.add_node(y)
            graph.add_edge(x, y, _predict_cost(x, y, event_name, date))

    # construct return
    (cost, path) = dijsktra(graph, origin_airport)

    pathes = []
    current_airport = destination_airport
    while current_airport is not origin_airport:
        pathes.append(path[current_airport])
        current_airport = path[current_airport]
    pathes = [x for x in reversed(pathes)]

    list_cost = dict( [ (dest, cost[dest]) for dest in pathes ] )
    list_cost.pop(origin_airport, None)

    # edge case: destination_airport might not in the result..
    return (
        list_cost,
        pathes
    )

def _predict_cost(this_airport, that_airport, event_name, date):
    """

    :param from_city: string
    :param to_city: string
    :return:
    """
    # TODO: predict cost using the model
    return random.randint(1, 101)

def _predict_time(event_name, from_city, to_city, date):
    """

    :param from_city: string
    :param to_city: string
    :return: time, in milliseconds
    """
    100
    pass

def _predict_delay(event_name, from_city, to_city, date):
    """

    :param from_city: string
    :param to_city: string
    :return: time, in milliseconds
    """
    pass