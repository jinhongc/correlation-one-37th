
import random

airport_id_routes = {'GRI': {'DFW', 'IAH'}, 'IAH': { 'FAX', 'SFO' }, 'SFO': { 'XXX' }}


import collections

class Graph:
    def __init__(self):
        self.nodes = set()
        self.edges = collections.defaultdict(list)
        self.distances = {}

    def add_node(self, value):
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, distance):
        self.edges[from_node].append(to_node)
        self.distances[(from_node, to_node)] = distance


def dijsktra(graph, initial):

    # destnation: cost map
    visited = {initial: 0}

    # destination_airport: from_airport map
    path = {}

    nodes = set(graph.nodes)

    while nodes:
        min_node = None
        for node in nodes:
            if node in visited:
                if min_node is None:
                    min_node = node
                elif visited[node] < visited[min_node]:
                    min_node = node

        if min_node is None:
            break

        nodes.remove(min_node)
        current_weight = visited[min_node]

        for edge in graph.edges[min_node]:
            weight = current_weight + graph.distances[(min_node, edge)]
            if edge not in visited or weight < visited[edge]:
                visited[edge] = weight
                path[edge] = min_node

    return visited, path


def _predict_cost(this_airport, that_airport, event_name, date):
    """

    :param from_city: string
    :param to_city: string
    :return:
    """
    # TODO: predict cost using the model
    return random.randint(1, 101)


def _search_optimal_cost(origin_airport, destination_airport, event_name, date='2018-01-29'):
    """
    search routes from origin_city to destination_city
    :param citorigin_city:
    :param destination_city:
    :param date:
    :return:
    """
    graph = Graph()

    for x in airport_id_routes:
        graph.add_node(x)
        for y in airport_id_routes[x]:
            graph.add_node(y)
            graph.add_edge(x, y, _predict_cost(x, y, event_name, date))

    # construct return
    (cost, path) = dijsktra(graph, origin_airport)

    pathes = []
    current_airport = destination_airport
    while current_airport is not origin_airport:
        pathes.append(path[current_airport])
        current_airport = path[current_airport]
    pathes = [x for x in reversed(pathes)]

    list_cost = dict( [ (dest, cost[dest]) for dest in pathes ] )
    list_cost.pop(origin_airport, None)

    # edge case: destination_airport might not in the result..
    return (
        list_cost,
        pathes
    )

print(_search_optimal_cost('GRI', 'XXX', 'big event'))
