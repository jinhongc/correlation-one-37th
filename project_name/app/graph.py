import collections

class Graph:
    def __init__(self):
        self.nodes = set()
        self.edges = collections.defaultdict(list)
        self.distances = {}

    def add_node(self, value):
        self.nodes.add(value)

    def add_edge(self, from_node, to_node, distance):
        self.edges[from_node].append(to_node)
        self.distances[(from_node, to_node)] = distance


def dijsktra(graph, initial):

    # destnation: cost map
    visited = {initial: 0}

    # destination_airport: from_airport map
    path = {}

    nodes = set(graph.nodes)

    while nodes:
        min_node = None
        for node in nodes:
            if node in visited:
                if min_node is None:
                    min_node = node
                elif visited[node] < visited[min_node]:
                    min_node = node

        if min_node is None:
            break

        nodes.remove(min_node)
        current_weight = visited[min_node]

        for edge in graph.edges[min_node]:
            weight = current_weight + graph.distances[(min_node, edge)]
            if edge not in visited or weight < visited[edge]:
                visited[edge] = weight
                path[edge] = min_node

    return visited, path

#
# usage:
# g = Graph()
# g.add_node('SFO')
# g.add_node('IAX')
# g.add_node('PGH')
# g.add_node('XXX')
# g.add_edge('SFO', 'IAX', 100)
# g.add_edge('SFO', 'PGH', 101)
# g.add_edge('IAX', 'XXX', 101)
# g.add_edge('PGH', 'XXX', 10)
# dijsktra(g, 'SFO')
