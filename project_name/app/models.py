from django.contrib.auth.models import AbstractUser
import pandas
import re

# df1['origin_airport'].unique

print("preparing data...")

# origin_data_path = "/Users/jchen/git/django-project-template/data/origin"
# airports_csv = pandas.read_csv(origin_data_path + "/airports.csv")
# events_csv = pandas.read_csv(origin_data_path + "/events_US.csv")
# flight_traffic_csv = pandas.read_csv(origin_data_path + "/flight_traffic.csv")
#
# airport_id_to_city = dict(airports_csv[['airport_id','city']].drop_duplicates().values.tolist())
# city_to_airport_id = dict(airports_csv[['city','airport_id']].drop_duplicates().values.tolist())
# event_name_to_city = dict(events_csv[['event_name','city']].drop_duplicates().values.tolist())


# airport_id_routes = {}
# for item in flight_traffic_csv[['origin_airport', 'destination_airport']].values.tolist():
#     if item[0] not in airport_id_routes:
#         airport_id_routes.update({item[0]: {item[1]}})
#     else:
#         airport_id_routes[item[0]].add(item[1])


# events = [
#     { 'id': re.sub('[^0-9a-zA-Z]+', '_', name[0].lower()), 'display_name': name[0] }
#     for name in events_csv[['event_name']].drop_duplicates().values.tolist()
# ]

# cities = [
#     { 'id': re.sub('[^0-9a-zA-Z]+', '_', name.lower()), 'display_name': name }
#     for name in events_csv['city'].drop_duplicates().values.tolist()
# ]


events = [
    { 'id': 'x', 'display_name': 'x' }
]

cities = [
    { 'id': 'y', 'display_name': 'y' }
]

airport_id_routes = {'GRI': {'DFW', 'IAH'}, 'IAH': { 'FAX', 'SFO' }, 'SFO': { 'XXX' }}



print("preparing data done.")